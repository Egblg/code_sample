object Sdk {
    const val MIN_SDK_VERSION = 23
    const val TARGET_SDK_VERSION = 29
    const val COMPILE_SDK_VERSION = 29
}

object BuildPluginsVersion {
    const val AGP = "4.0.1"
    const val DETEKT = "1.11.0"
    const val KOTLIN = "1.4.0"
    const val KTLINT = "9.3.0"
    const val VERSIONS_PLUGIN = "0.29.0"
}

object UiVersion {
    const val GROPIE = "2.8.1"
    const val PinEntryEditText = "2.0.6"
    const val VercodeEditText = "1.1.0"
    const val DECORO = "1.4.1"
    const val COIL = "0.13.0"
    const val MaterialDialogs = "3.2.1"
    const val RECYCLICAL = "1.1.1"
    const val FLEX_BOX_LAYOUT = "2.0.1"
    const val GLIDE = "4.11.0"
}

object UiLibs {
    const val groupie = "com.xwray:groupie:${UiVersion.GROPIE}"
    const val groupieExtensions = "com.xwray:groupie-kotlin-android-extensions:${UiVersion.GROPIE}"
    const val groupieViewBinding = "com.github.lisawray.groupie:groupie-viewbinding:2.8.0"
    const val pinEntryEditText =
        "com.alimuzaffar.lib:pinentryedittext:${UiVersion.PinEntryEditText}"
    const val VercodeEditText = "com.justkiddingbaby:vercodeedittext:${UiVersion.VercodeEditText}"

    const val Decoro = "ru.tinkoff.decoro:decoro:${UiVersion.DECORO}"
    const val Coil = "io.coil-kt:coil:${UiVersion.COIL}"
    const val MaterialDialogs = "com.afollestad.material-dialogs:core:${UiVersion.MaterialDialogs}"
    const val MaterialDialogsFiles =
        "com.afollestad.material-dialogs:files:${UiVersion.MaterialDialogs}"
    const val RECYCLICAL = "com.afollestad:recyclical:${UiVersion.RECYCLICAL}"
    const val FLEX_BOX_LAYOUT = "com.google.android:flexbox:${UiVersion.FLEX_BOX_LAYOUT}"
    const val GLIDE = "com.github.bumptech.glide:glide:${UiVersion.GLIDE}"
    const val GLIDE_COMPILER = "com.github.bumptech.glide:compiler:${UiVersion.GLIDE}"
}

object DataVersion {
    const val RETROFIT = "2.9.0"
    const val RETROFIT_ADAPTER = "0.7.0"
    const val INTERCEPTOR = "4.7.2"
    const val ROOM = "2.2.5"
}

object DataLibs {
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${DataVersion.RETROFIT}"
    const val RETROFIT_ADAPTER =
        "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${DataVersion.RETROFIT_ADAPTER}"
    const val INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:${DataVersion.INTERCEPTOR}"
    const val ROOM = "androidx.room:room-runtime:${DataVersion.ROOM}"
    const val ROOM_COMPILER = "androidx.room:room-compiler:${DataVersion.ROOM}"
    const val ROOM_EXT = "androidx.room:room-ktx:${DataVersion.ROOM}"
}

object PresentationVersion {
    const val MOXY = "2.1.2"
    const val CICERONE = "5.1.1"
}

object PresentationLibs {
    const val moxy = "com.github.moxy-community:moxy:${PresentationVersion.MOXY}"
    const val moxyAndroid = "com.github.moxy-community:moxy-android:${PresentationVersion.MOXY}"
    const val moxyAndroidX = "com.github.moxy-community:moxy-androidx:${PresentationVersion.MOXY}"
    const val moxyMaterial = "com.github.moxy-community:moxy-material:${PresentationVersion.MOXY}"
    const val moxyCompiler = "com.github.moxy-community:moxy-compiler:${PresentationVersion.MOXY}"
    const val moxyDelegate = "com.github.moxy-community:moxy-ktx:${PresentationVersion.MOXY}"

    const val CICERONE = "ru.terrakok.cicerone:cicerone:${PresentationVersion.CICERONE}"
}

object LoggingVersion {
    const val TIMBER = "4.7.1"
    const val CRASHLYTICS = "2.10.1"
}

object LoggingLibs {
    const val crashlytics = "com.crashlytics.sdk.android:crashlytics:${LoggingVersion.CRASHLYTICS}"

    const val timber = "com.jakewharton.timber:timber:${LoggingVersion.TIMBER}"
}

object ExtensionsVersion {
    const val SPLITTIES = "3.0.0-alpha06"
}

object ExtensionsLib {
    const val EXTENSIONS_RESOURCES =
        "com.louiscad.splitties:splitties-resources:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_TOAST =
        "com.louiscad.splitties:splitties-toast:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_ALERT =
        "com.louiscad.splitties:splitties-alertdialog:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_DIMENSION =
        "com.louiscad.splitties:splitties-dimensions:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_PERMISIONS =
        "com.louiscad.splitties:splitties-permissions:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_PREFERENCES =
        "com.louiscad.splitties:splitties-preferences:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_SNACK_BAR =
        "com.louiscad.splitties:splitties-snackbar:${ExtensionsVersion.SPLITTIES}"
    const val EXTENSIONS_VIEWS =
        "com.louiscad.splitties:splitties-views:${ExtensionsVersion.SPLITTIES}"
}

object Versions {
    const val CORE_KTX = "1.3.1"
    const val APPCOMPAT = "1.2.0"
    const val CONSTRAINT_LAYOUT = "2.0.0"
    const val ESPRESSO_CORE = "3.2.0"
    const val JUNIT = "4.13"
    const val KTLINT = "0.37.2"
    const val ANDROIDX_TEST = "1.2.0"
    const val ANDROIDX_TEST_EXT = "1.1.1"
    const val MATERIAL = "1.2.0"
    const val KOIN = "2.1.5"
    const val VIEW_BINDING_PROPERTY_DELEGATE = "1.0.0"
    const val LEGACY = "1.0.0"
}

object SupportLibs {
    const val ANDROIDX_APPCOMPAT = "androidx.appcompat:appcompat:${Versions.APPCOMPAT}"
    const val ANDROIDX_CONSTRAINT_LAYOUT =
        "com.android.support.constraint:constraint-layout:${Versions.CONSTRAINT_LAYOUT}"
    const val ANDROIDX_CORE_KTX = "androidx.core:core-ktx:${Versions.CORE_KTX}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.CORE_KTX}"
    const val MATERIAL = "com.google.android.material:material:${Versions.MATERIAL}"
    const val KOIN = "org.koin:koin-android:${Versions.KOIN}"
    const val KOIN_INSTANCE = "org.koin:koin-core:${Versions.KOIN}"
    const val KOIN_SCOPE = "org.koin:koin-android-scope:${Versions.KOIN}"
    const val KOIN_VIEW_MODEL = "org.koin:koin-android-viewmodel:${Versions.KOIN}"
    const val VIEW_BINDING_PROPERTY_DELEGATE =
        "com.kirich1409.viewbindingpropertydelegate:viewbindingpropertydelegate:${Versions.VIEW_BINDING_PROPERTY_DELEGATE}"
    const val FIREBASE = "com.google.firebase:firebase-bom:25.12.0"
    const val FIREBASE_MESSAGING = "com.google.firebase:firebase-messaging:20.3.0"
    const val FIREBASE_ANALYTICS = "com.google.firebase:firebase-analytics:17.6.0"
    const val FIREBASE_CRASHLYTICS = "com.google.firebase:firebase-crashlytics:17.2.2"
    const val LEGACY = "androidx.legacy:legacy-support-v4:${Versions.LEGACY}"
}

object TestingLib {
    const val JUNIT = "junit:junit:${Versions.JUNIT}"
}

object AndroidTestingLib {
    const val ANDROIDX_TEST_RULES = "androidx.test:rules:${Versions.ANDROIDX_TEST}"
    const val ANDROIDX_TEST_RUNNER = "androidx.test:runner:${Versions.ANDROIDX_TEST}"
    const val ANDROIDX_TEST_EXT_JUNIT = "androidx.test.ext:junit:${Versions.ANDROIDX_TEST_EXT}"
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:${Versions.ESPRESSO_CORE}"
}

object PaymentVersion {
    const val YANDEX_CASSA = "3.1.0"
}

object PaymentLibs {
    const val YANDEX_CASSA = "com.yandex.money:checkout:${PaymentVersion.YANDEX_CASSA}"
}
