val PUBLISHING_GROUP = "com.egblg.apps.bars"

object AppCoordinates {
    const val APP_ID = "com.egblg.apps.bars"
    const val APP_VERSION_NAME = "1.0.0"
    const val SERVICE_APP_VERSION_NAME = "1.0.0"
    const val APP_VERSION_CODE = 1
    const val SERVICE_APP_VERSION_CODE = 1
}

object LibraryAndroidCoordinates {
    const val LIBRARY_VERSION = "1.0"
    const val LIBRARY_VERSION_CODE = 1
}

object LibraryKotlinCoordinates {
    const val LIBRARY_VERSION = "1.0"
}
