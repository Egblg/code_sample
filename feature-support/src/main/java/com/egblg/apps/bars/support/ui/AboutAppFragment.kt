package com.egblg.apps.bars.support.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.withItem
import com.egblg.apps.bars.common.ui.BaseFragment
import com.egblg.apps.bars.support.R
import com.egblg.apps.bars.support.data.AboutAppData
import com.egblg.apps.bars.support.presentation.aboutApp.AboutAppPresenter
import com.egblg.apps.bars.support.presentation.aboutApp.AboutAppView
import com.egorius.support.databinding.FragmentAboutAppBinding
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get

class AboutAppFragment : BaseFragment(R.layout.fragment_about_app), AboutAppView {

    private val binding: FragmentAboutAppBinding by viewBinding()

    @InjectPresenter
    lateinit var presenter: AboutAppPresenter

    @ProvidePresenter
    fun providePresenter() = get<AboutAppPresenter>()

    private val supportItemSource = emptyDataSourceTyped<SupportItem>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setupRv()
    }

    private fun setupRv() {
        binding.aboutAppRv.apply {
            addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))
            setup {
                withDataSource(supportItemSource)
                withItem<SupportItem, SupportViewHolder>(R.layout.layout_support_button) {
                    onBind(::SupportViewHolder) { _, item ->
                        title.text = item.name
                    }
                    onClick {
                        val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse(item.url))
                        startActivity(browserIntent)
                    }
                }
            }
        }
    }

    private fun setListeners() {
        with(binding) {
            layoutAppBar.toolbarTitle.setText(R.string.about_app)
            layoutAppBar.backButton.onClick { presenter.onBackPressed() }
        }
    }

    override fun showAboutApp(data: AboutAppData) {
        supportItemSource.addAll(data.map { SupportItem(name = it.name, url = it.url) })
    }

    override fun toggleLoading(isVisible: Boolean) {
        binding.loading.root.visible(isVisible)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
