package com.egblg.apps.bars.support.presentation.settings

import com.egblg.apps.bars.auth.data.RegionItem
import com.egblg.apps.bars.common.presentation.BaseView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface SettingsView : BaseView {
    fun showSavedRegion(savedRegion: RegionItem)
    fun showRegionPlaceHolder()
    fun setupRegionDialog(data: List<RegionItem>, savedRegion: RegionItem?)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showRegionDialog()
    fun toggleLoading(isVisible: Boolean)
}
