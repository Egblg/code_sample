package com.egblg.apps.bars.support.di

import com.egblg.apps.bars.support.domain.SupportInteractorImpl
import com.egblg.apps.bars.support.presentation.supportFlow.SupportServiceFlowPresenter
import com.egblg.apps.bars.support.presentation.supportForm.SupportFormPresenter
import org.koin.dsl.module

val supportModule = module {
    single { SupportServiceFlowPresenter(router = get(), screens = get(), interactor = get()) }
    single { SupportInteractorImpl(supportRepository = get()) }
    single { SupportFormPresenter(router = get(), supportInteractor = get()) }
}
