package com.egblg.apps.bars.support.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.afollestad.recyclical.itemdefinition.onChildViewClick
import com.afollestad.recyclical.withItem
import com.egblg.apps.bars.common.ui.BaseFragment
import com.egblg.apps.bars.common.utils.args
import com.egblg.apps.bars.common.utils.getFileName
import com.egblg.apps.bars.common.utils.hideKeyboard
import com.egblg.apps.bars.common.utils.showSnackbar
import com.egblg.apps.bars.support.R
import com.egblg.apps.bars.support.presentation.supportForm.SupportFormPresenter
import com.egblg.apps.bars.support.presentation.supportForm.SupportFormView
import com.egorius.support.databinding.FragmentSupportFormBinding
import kotlinx.coroutines.launch
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import splitties.lifecycle.coroutines.lifecycleScope
import splitties.permissions.PermissionRequestResult
import splitties.permissions.hasPermission
import splitties.permissions.requestPermission
import splitties.resources.str
import splitties.toast.toast

private const val PICKER_CODE = 2

class SupportFormFragment : BaseFragment(R.layout.fragment_support_form), SupportFormView {

    private val binding: FragmentSupportFormBinding by viewBinding()

    @InjectPresenter
    lateinit var presenter: SupportFormPresenter

    @ProvidePresenter
    fun providePresenter() = get<SupportFormPresenter>()

    private val fileSource = emptyDataSourceTyped<FileItem>()

    companion object {
        fun create(type: String) = SupportFormFragment().apply {
            this.type = type
        }
    }

    private var type: String by args()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        setLabels()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.filesRv.setup {
            withDataSource(fileSource)
            withItem<FileItem, FileViewHolder>(R.layout.file_item) {
                onBind(::FileViewHolder) { _, item ->
                    name.text = item.name
                }
                onChildViewClick(FileViewHolder::deleteButton) { _, _ ->
                    presenter.onDeleteFile(item.uri)
                    fileSource.remove(item)
                    if (fileSource.isEmpty()) {
                        binding.filesRv.visible(false)
                    }
                }
            }
        }
    }

    override fun showFileName(uri: Uri) {
        val fileName = uri.getFileName(requireContext())
        if (fileName != null) {
            fileSource.add(FileItem(uri, fileName))
            binding.filesRv.visible(true)
        }
    }

    private fun setLabels() {
        binding.layoutAppBar.toolbarTitle.setText(R.string.support_service)
    }

    private fun setListeners() {
        with(binding) {
            emailEditText.doAfterTextChanged { presenter.onEmailEntered(it.toString()) }
            contentEditText.doAfterTextChanged { presenter.onContentEntered(it.toString()) }
            sendButton.onClick { presenter.onSendClick(type) }
            layoutAppBar.backButton.onClick {
                hideKeyboard()
                presenter.onBackPressed()
            }
            clipFileButton.onClick { openPhotoPicker() }
        }
    }

    override fun requestPermissions() {
        lifecycleScope.launch {
            if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                presenter.permissionsGranted()
            } else {
                when (requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    is PermissionRequestResult.Granted -> presenter.permissionsGranted()

                    is PermissionRequestResult.Denied.MayAskAgain -> showSnackbar(
                        str(R.string.may_ask_again_file_permission),
                        true
                    )
                    is PermissionRequestResult.Denied.DoNotAskAgain ->
                        showSnackbar(str(R.string.do_not_ask_again_file_permission), true)
                }
            }
        }
    }

    override fun clearShowingFiles() {
        fileSource.clear()
        binding.filesRv.visible(false)
    }

    override fun openPhotoPicker() {
        val photoPickerIntent = Intent(Intent.ACTION_GET_CONTENT)
        photoPickerIntent.type = "image/*"
        startActivityForResult(photoPickerIntent, PICKER_CODE)
    }

    override fun onBackPressed() {
        hideKeyboard()
        presenter.onBackPressed()
    }

    override fun showSuccessMsg() {
        MaterialDialog(requireContext()).show {
            title(R.string.appeal_accepted)
            positiveButton { presenter.onSuccessOkClick() }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PICKER_CODE -> {
                if (resultCode == Activity.RESULT_OK && data?.data != null) {
                    presenter.onFileClipped(data.data)
                }
            }
        }
    }

    override fun showNoContentMsg() {
        toast(R.string.no_content_msg)
    }

    override fun showIncorrectEmailMsg() {
        toast(R.string.incorrect_email)
    }
}
