package com.egblg.apps.bars.support.ui

import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.customListAdapter
import com.egblg.apps.bars.auth.data.RegionItem
import com.egblg.apps.bars.common.ui.BaseFragment
import com.egblg.apps.bars.common.ui.CustomDialogListAdapter
import com.egblg.apps.bars.common.utils.args
import com.egblg.apps.bars.support.R
import com.egblg.apps.bars.support.presentation.settings.SettingsPresenter
import com.egblg.apps.bars.support.presentation.settings.SettingsView
import com.egorius.support.databinding.FragmentSettingsBinding
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get

class SettingsFragment : BaseFragment(R.layout.fragment_settings), SettingsView {

    private val binding: FragmentSettingsBinding by viewBinding()

    private lateinit var regionDialog: MaterialDialog

    @InjectPresenter
    lateinit var presenter: SettingsPresenter

    @ProvidePresenter
    fun providePresenter() = get<SettingsPresenter>()

    companion object {
        fun create(fromOrder: Boolean) = SettingsFragment().apply {
            this.fromOrder = fromOrder
        }
    }

    private var fromOrder: Boolean by args()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onCreate(fromOrder)
        with(binding) {
            setLabels()
            setListeners()
        }
    }

    private fun FragmentSettingsBinding.setLabels() {
        layoutAppBar.toolbarTitle.setText(R.string.settings)
    }

    private fun FragmentSettingsBinding.setListeners() {
        layoutAppBar.backButton.onClick { presenter.onBackPressed() }
        languageSpinner.onClick {
            MaterialDialog(requireContext()).show {
                title(R.string.app_language)
                listItems(R.array.app_languages) { _, _, _ ->
                }
            }
        }
        regionSpinner.onClick { showRegionDialog() }
    }

    override fun showSavedRegion(savedRegion: RegionItem) {
        binding.regionSpinner.text = savedRegion.name
    }

    override fun showRegionPlaceHolder() {
        binding.regionSpinner.setText(R.string.choose_region)
    }

    override fun setupRegionDialog(
        data: List<RegionItem>,
        savedRegion: RegionItem?
    ) {
        regionDialog = MaterialDialog(requireContext()).apply {
            title(R.string.choose_region)
            customListAdapter(CustomDialogListAdapter(data.map {
                Pair(
                    it.code,
                    it.name
                )
            }) { id, text ->
                dismiss()
                presenter.onRegionSelected(RegionItem(id, text))
            })
        }
        if (fromOrder) {
            showRegionDialog()
        }
    }

    override fun showRegionDialog() {
        if (::regionDialog.isInitialized) {
            regionDialog.show()
        }
    }

    override fun toggleLoading(isVisible: Boolean) {
        binding.loading.root.visible(isVisible)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
