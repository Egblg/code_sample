package com.egblg.apps.bars.support.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.afollestad.recyclical.datasource.emptyDataSourceTyped
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.withItem
import com.egblg.apps.bars.common.data.response.SupportStatementResponce
import com.egblg.apps.bars.common.ui.BaseFragment
import com.egblg.apps.bars.support.R
import com.egblg.apps.bars.support.presentation.supportFlow.SupportServiceFlowPresenter
import com.egblg.apps.bars.support.presentation.supportFlow.SupportServiceFlowView
import com.egorius.support.databinding.FragmentSupportServiceFlowBinding
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get

class SupportServiceFlowFragment : BaseFragment(R.layout.fragment_support_service_flow),
    SupportServiceFlowView {

    private val binding: FragmentSupportServiceFlowBinding by viewBinding()

    private val statementsSource = emptyDataSourceTyped<SupportStatement>()

    @InjectPresenter
    lateinit var presenter: SupportServiceFlowPresenter

    @ProvidePresenter
    fun providePresenter() = get<SupportServiceFlowPresenter>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRv()
        setupFields()
    }

    private fun setupFields() {
        with(binding) {
            layoutAppBar.toolbarTitle.setText(R.string.support_service)
            layoutAppBar.backButton.onClick { presenter.onBackPressed() }
        }
    }

    override fun showSupportStatements(data: List<SupportStatementResponce>) {
        statementsSource.clear()
        statementsSource.addAll(data.map { SupportStatement(it.name) })
    }

    private fun setupRv() {
        binding.rv.apply {
            addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))
            setup {
                withDataSource(statementsSource)
                withItem<SupportStatement, SupportStatementViewHolder>(R.layout.layout_support_button) {
                    onBind(::SupportStatementViewHolder) { _, item ->
                        title.text = item.title
                    }
                    onClick {
                        presenter.onItemClick(item.title)
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
