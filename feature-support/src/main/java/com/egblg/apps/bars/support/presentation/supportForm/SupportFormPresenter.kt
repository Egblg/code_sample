package com.egblg.apps.bars.support.presentation.supportForm

import android.net.Uri
import com.egblg.apps.bars.common.EMAIL_RFC_REG
import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.presentation.BasePresenter
import com.egblg.apps.bars.support.domain.SupportInteractor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
class SupportFormPresenter(
    private val router: Router,
    private val supportInteractor: SupportInteractor
) : BasePresenter<SupportFormView>() {

    private val viewModel = SupportFormViewModel()

    fun onSendClick(type: String) {
        if (viewModel.email.isNullOrEmpty() || viewModel.content.isNullOrEmpty() || viewModel.content.isNullOrBlank()) {
            viewState.showNoContentMsg()
        } else if (!EMAIL_RFC_REG.toRegex().matches(viewModel.email!!)) {
            viewState.showIncorrectEmailMsg()
        } else {
            presenterScope.launch {
                supportInteractor.sendAppeal(
                    viewModel.email!!,
                    viewModel.content!!,
                    if (viewModel.files.isNotEmpty()) viewModel.files else null,
                    type
                ).collect {
                    when (it) {
                        is Result.Success -> {
                            viewModel.clear()
                            viewState.clearShowingFiles()
                            viewState.showSuccessMsg()
                        }
                        is Result.Error -> viewState.showNetworkConnectionError()
                    }
                }
            }
        }
    }

    fun onDeleteFile(uri: Uri) {
        viewModel.files.remove(uri)
    }

    override fun onBackPressed() {
        router.exit()
    }

    fun onEmailEntered(email: String) {
        viewModel.email = email
    }

    fun onContentEntered(content: String) {
        viewModel.content = content
    }

    fun onSuccessOkClick() {
        router.exit()
    }

    fun permissionsGranted() {
        viewState.openPhotoPicker()
    }

    fun onFileClipped(data: Uri?) {
        if (data != null) {
            viewState.showFileName(data)
            viewModel.files.add(data)
        }
    }
}
