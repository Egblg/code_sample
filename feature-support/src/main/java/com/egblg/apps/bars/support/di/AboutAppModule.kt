package com.egblg.apps.bars.support.di

import com.egblg.apps.bars.support.presentation.aboutApp.AboutAppPresenter
import org.koin.dsl.module

val aboutAppModule = module {
    single { AboutAppPresenter(router = get(), interactor = get()) }
}
