package com.egblg.apps.bars.support.di

import com.egblg.apps.bars.support.presentation.settings.SettingsPresenter
import org.koin.dsl.module

val settingsModule = module {
    single { SettingsPresenter(router = get(), regionUseCase = get(), appType = get()) }
}
