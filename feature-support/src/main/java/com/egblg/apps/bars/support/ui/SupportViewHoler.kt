package com.egblg.apps.bars.support.ui

import android.view.View
import android.widget.TextView
import com.afollestad.recyclical.ViewHolder

data class SupportItem(
    val name: String,
    val url: String
)

class SupportViewHolder(itemView: View) : ViewHolder(itemView) {
    val title: TextView = itemView as TextView
}
