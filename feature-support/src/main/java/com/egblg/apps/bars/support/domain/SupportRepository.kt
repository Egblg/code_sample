package com.egblg.apps.bars.support.domain

import android.net.Uri
import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.data.response.SupportStatementResponce
import com.egblg.apps.bars.support.data.AboutAppData
import kotlinx.coroutines.flow.Flow

interface SupportRepository {
    suspend fun sendAppeal(
        email: String,
        content: String,
        files: List<Uri>?,
        type: String
    ): Flow<Result<String>>

    suspend fun getAboutApp(): Flow<Result<AboutAppData>>
    suspend fun getSupportStatements(): Flow<Result<List<SupportStatementResponce>>>
}
