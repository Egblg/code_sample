package com.egblg.apps.bars.support.presentation.supportFlow

import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.presentation.BasePresenter
import com.egblg.apps.bars.support.domain.SupportInteractor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
class SupportServiceFlowPresenter(
    private val router: Router,
    private val screens: SupportFlowScreens,
    private val interactor: SupportInteractor
) : BasePresenter<SupportServiceFlowView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        presenterScope.launch {
            interactor.getSupportStatements().collect {
                when (it) {
                    is Result.Success -> viewState.showSupportStatements(it.data)
                    is Result.Error -> viewState.showNetworkConnectionError()
                }
            }
        }
    }

    fun onItemClick(title: String) {
        router.navigateTo(screens.showFormScreen(title))
    }

    override fun onBackPressed() {
        router.exit()
    }
}
