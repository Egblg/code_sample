package com.egblg.apps.bars.support.domain

import android.net.Uri

class SupportInteractorImpl(private val supportRepository: SupportRepository) : SupportInteractor {

    override suspend fun getSupportStatements() = supportRepository.getSupportStatements()

    override suspend fun sendAppeal(
        email: String,
        content: String,
        files: List<Uri>?,
        type: String
    ) =
        supportRepository.sendAppeal(email, content, files, type)

    override suspend fun getAboutApp() = supportRepository.getAboutApp()
}
