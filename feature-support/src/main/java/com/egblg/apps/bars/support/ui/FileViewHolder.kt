package com.egblg.apps.bars.support.ui

import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.afollestad.recyclical.ViewHolder
import com.egblg.apps.bars.support.R

data class FileItem(
    val uri: Uri,
    val name: String
)

class FileViewHolder(itemView: View) : ViewHolder(itemView) {
    val name: TextView = itemView.findViewById(R.id.text_view)
    val deleteButton: ImageView = itemView.findViewById(R.id.delete_button)
}
