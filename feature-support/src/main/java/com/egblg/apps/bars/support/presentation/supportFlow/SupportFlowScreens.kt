package com.egblg.apps.bars.support.presentation.supportFlow

import ru.terrakok.cicerone.android.support.SupportAppScreen

interface SupportFlowScreens {
    fun showFormScreen(type: String): SupportAppScreen
}
