package com.egblg.apps.bars.support.data

typealias AboutAppData = List<AboutAppItem>

data class AboutAppItem(
    val id: Int,
    val name: String,
    val url: String
)
