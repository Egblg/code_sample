package com.egblg.apps.bars.support.presentation.supportFlow

import com.egblg.apps.bars.common.data.response.SupportStatementResponce
import com.egblg.apps.bars.common.presentation.BaseView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface SupportServiceFlowView : BaseView {
    fun showSupportStatements(data: List<SupportStatementResponce>)
}
