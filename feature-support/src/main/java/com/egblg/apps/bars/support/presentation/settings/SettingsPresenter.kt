package com.egblg.apps.bars.support.presentation.settings

import com.egblg.apps.bars.auth.data.RegionItem
import com.egblg.apps.bars.auth.domain.RegionUseCase
import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.domain.AppType
import com.egblg.apps.bars.common.presentation.BasePresenter
import kotlinx.coroutines.launch
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
class SettingsPresenter(
    private val router: Router,
    private val regionUseCase: RegionUseCase,
    private val appType: AppType
) : BasePresenter<SettingsView>() {

    private var isAutoExit: Boolean? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        if (appType == AppType.CLIENT) {
            val savedRegion = regionUseCase.getSavedRegion()
            if (savedRegion != null) {
                viewState.showSavedRegion(savedRegion)
            } else {
                viewState.showRegionPlaceHolder()
            }
            presenterScope.launch {
                viewState.toggleLoading(true)
                regionUseCase.getRegions().collect {
                    viewState.toggleLoading(false)
                    when (it) {
                        is Result.Success -> viewState.setupRegionDialog(it.data, savedRegion)
                        is Result.Error -> viewState.showNetworkConnectionError()
                    }
                }
            }
        }
    }

    fun onCreate(isAutoExit: Boolean) {
        this.isAutoExit = isAutoExit
    }

    override fun onBackPressed() {
        router.exit()
    }

    fun onRegionSelected(regionItem: RegionItem) {
        presenterScope.launch {
            regionUseCase.saveRegion(regionItem.code, regionItem.name)
        }
        viewState.showSavedRegion(regionItem)
        if (isAutoExit != null && isAutoExit == true) {
            router.exit()
        }
    }
}
