package com.egblg.apps.bars.support.presentation.supportForm

import android.net.Uri
import com.egblg.apps.bars.common.presentation.BaseView
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(OneExecutionStateStrategy::class)
interface SupportFormView : BaseView {
    fun showNoContentMsg()
    fun showIncorrectEmailMsg()
    fun showSuccessMsg()
    fun requestPermissions()
    fun showFileName(uri: Uri)
    fun openPhotoPicker()
    fun clearShowingFiles()
}
