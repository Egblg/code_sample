package com.egblg.apps.bars.support.presentation.aboutApp

import com.egblg.apps.bars.common.presentation.BaseView
import com.egblg.apps.bars.support.data.AboutAppData
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface AboutAppView : BaseView {
    fun toggleLoading(isVisible: Boolean)
    fun showAboutApp(data: AboutAppData)
}
