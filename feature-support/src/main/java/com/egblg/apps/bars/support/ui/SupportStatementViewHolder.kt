package com.egblg.apps.bars.support.ui

import android.view.View
import android.widget.TextView
import com.afollestad.recyclical.ViewHolder
import com.egblg.apps.bars.support.R

data class SupportStatement(val title: String)

class SupportStatementViewHolder(itemView: View) : ViewHolder(itemView) {
    val title: TextView = itemView.findViewById(R.id.support_button)
}
