package com.egblg.apps.bars.support.presentation.supportForm

import android.net.Uri

data class SupportFormViewModel(
    var email: String? = null,
    var content: String? = null,
    val files: ArrayList<Uri> = ArrayList()
) {
    fun clear() {
        email = null
        content = null
        files.clear()
    }
}
