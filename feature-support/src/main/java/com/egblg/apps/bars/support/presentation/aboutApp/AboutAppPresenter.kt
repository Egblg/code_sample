package com.egblg.apps.bars.support.presentation.aboutApp

import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.presentation.BasePresenter
import com.egblg.apps.bars.support.domain.SupportInteractor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
class AboutAppPresenter(
    private val router: Router,
    private val interactor: SupportInteractor
) : BasePresenter<AboutAppView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        presenterScope.launch {
            viewState.toggleLoading(true)
            interactor.getAboutApp().collect {
                viewState.toggleLoading(false)
                when (it) {
                    is Result.Success -> viewState.showAboutApp(it.data)
                    is Result.Error -> viewState.showNetworkConnectionError()
                }
            }
        }
    }

    override fun onBackPressed() {
        router.exit()
    }
}
