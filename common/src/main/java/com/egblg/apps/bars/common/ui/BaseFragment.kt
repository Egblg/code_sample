package com.egblg.apps.bars.common.ui

import androidx.annotation.LayoutRes
import com.egblg.apps.bars.R
import moxy.MvpAppCompatFragment

import splitties.toast.longToast

abstract class BaseFragment(@LayoutRes contentLayoutId: Int) :
    MvpAppCompatFragment(contentLayoutId) {

    fun showNetworkConnectionError() = longToast(R.string.network_connection_error)

    fun showUnknownError() = longToast(R.string.unknown_error)

    abstract fun onBackPressed()
}
