package com.egblg.apps.bars.common.domain

interface NavigationProvider {
    fun showDrawer()
}
