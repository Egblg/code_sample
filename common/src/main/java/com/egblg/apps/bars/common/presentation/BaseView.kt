package com.egblg.apps.bars.common.presentation

import moxy.MvpView
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface BaseView : MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun showNetworkConnectionError()

    @StateStrategyType(SkipStrategy::class)
    fun showUnknownError()
}
