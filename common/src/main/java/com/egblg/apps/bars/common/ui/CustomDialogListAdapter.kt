package com.egblg.apps.bars.common.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.egblg.apps.bars.R
import splitties.views.onClick

class CustomDialogListAdapter(
    private val data: List<Pair<Int, String>>,
    private val listener: (id: Int, text: String) -> Unit
) : RecyclerView.Adapter<CustomDialogListAdapter.CustomViewHolder>() {

    class CustomViewHolder(private val view: TextView) :
        RecyclerView.ViewHolder(view) {
        fun bind(pair: Pair<Int, String>, listener: (id: Int, text: String) -> Unit) {
            view.onClick { listener(pair.first, pair.second) }
            view.text = pair.second
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.dialog_list_item, parent, false) as TextView
        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(data[position], listener)
    }

    override fun getItemCount(): Int = data.size
}
