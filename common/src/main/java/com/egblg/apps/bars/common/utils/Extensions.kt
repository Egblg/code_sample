package com.egblg.apps.bars.common.utils

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace
import java.util.Locale

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.invisible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.INVISIBLE
}

val Context.inputMethodManager: InputMethodManager?
    get() = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

fun Activity.hideKeyboard() {
    currentFocus?.windowToken?.let {
        inputMethodManager?.hideSoftInputFromWindow(it, 0)
    }
}

fun View.showKeyboard() {
    requestFocus()
    findFocus()?.let {
        context?.inputMethodManager?.showSoftInput(it, 0)
    }
}

fun Fragment.showKeyboard() {
    view?.findFocus()?.let {
        context?.inputMethodManager?.showSoftInput(it, 0)
    }
}

fun Fragment.hideKeyboard() {
    view?.findFocus()?.windowToken?.let {
        context?.inputMethodManager?.hideSoftInputFromWindow(it, 0)
    }
}

fun Fragment.showSnackbar(@StringRes messageResId: Int, isLong: Boolean = false) {
    view?.let {
        Snackbar.make(
            it,
            messageResId,
            if (isLong) Snackbar.LENGTH_LONG
            else Snackbar.LENGTH_SHORT
        ).show()
    }
}

fun Fragment.showSnackbar(message: String, isLong: Boolean = false) {
    view?.let {
        Snackbar.make(
            it,
            message,
            if (isLong) Snackbar.LENGTH_LONG
            else Snackbar.LENGTH_SHORT
        ).show()
    }
}

fun Uri?.getFileName(context: Context): String? {
    if (this?.scheme == "content") {
        context.contentResolver.query(this, null, null, null, null)?.use { cursor ->
            val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            cursor.moveToFirst()
            return cursor.getString(nameIndex)
        }
    }
    return null
}

fun TextView.drawableRight(@DrawableRes drawableId: Int) {
    this.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawableId, 0)
}

fun Navigator.start(initScreen: SupportAppScreen) {
    this.applyCommands(
        arrayOf(
            BackTo(null),
            Replace(initScreen)
        )
    )
}

fun Int.toNumberFormat(): String = "%,d".format(Locale.getDefault(), this)

