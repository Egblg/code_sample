package com.egblg.apps.bars.common.presentation

import kotlinx.coroutines.channels.ReceiveChannel

class ReceiveChannelComposer {
    private val receiveChannelSet = HashSet<ReceiveChannel<*>>()

    fun connect(receiveChannel: ReceiveChannel<*>) {
        receiveChannelSet.add(receiveChannel)
    }

    fun cancelAll() {
        receiveChannelSet.forEach {
            it.cancel()
        }
        receiveChannelSet.clear()
    }
}
