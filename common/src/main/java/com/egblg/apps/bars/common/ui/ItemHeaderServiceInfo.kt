package com.egblg.apps.bars.common.ui

import android.content.Context
import android.view.View
import com.egblg.apps.bars.R
import com.sixhands.twobars.databinding.ItemServiceInfoBinding
import com.xwray.groupie.viewbinding.BindableItem

class ItemHeaderServiceInfo(
    private val id: Int,
    private val title: String?,
    private val coefficients: List<Pair<String, String>>,
    private val context: Context
) :
    BindableItem<ItemServiceInfoBinding>(id.toLong()) {
    override fun bind(viewBinding: ItemServiceInfoBinding, position: Int) {
        with(viewBinding) {
            nameTextView.text = String.format(
                context.str(R.string.service_placeholder),
                abs(this@ItemHeaderServiceInfo.id)
            )
            valueTextView.text = title
            infoTextView.text = coefficients.joinToString(
                separator = "\n",
                transform = { "${it.first}: ${it.second}" })
        }
    }

    override fun getLayout(): Int = R.layout.item_service_info

    override fun initializeViewBinding(view: View): ItemServiceInfoBinding =
        ItemServiceInfoBinding.bind(view)
}
