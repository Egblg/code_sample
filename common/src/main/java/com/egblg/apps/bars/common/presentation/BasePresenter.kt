package com.egblg.apps.bars.common.presentation

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.launch
import moxy.MvpPresenter
import moxy.presenterScope

abstract class BasePresenter<T : BaseView> : MvpPresenter<T>() {
    private var job: Job = Job()
    protected var presenterScope = CoroutineScope(job + Dispatchers.Main)
    private val receiveChannelComposer = ReceiveChannelComposer()

    protected fun <T> ReceiveChannel<T>.connect() = apply {
        receiveChannelComposer.connect(this)
    }

    override fun onDestroy() {
        receiveChannelComposer.cancelAll()
        super.onDestroy()
    }

    abstract fun onBackPressed()
}

fun <V : BaseView> BasePresenter<V>.launch(block: suspend CoroutineScope.() -> Unit) =
    presenterScope.launch { block() }
