package com.egblg.apps.bars.common.domain

enum class AppType {
    CLIENT, SERVICE
}
