package com.egblg.apps.bars.common.ui

import android.view.View
import com.egblg.apps.bars.R
import com.sixhands.twobars.databinding.ItemTextWithPriceBinding
import com.xwray.groupie.viewbinding.BindableItem

class ItemTextWithPrice(private val title: String, private val price: Int?) :
    BindableItem<ItemTextWithPriceBinding>() {
    override fun bind(viewBinding: ItemTextWithPriceBinding, position: Int) {
        with(viewBinding) {
            nameTextView.text = title
            priceTextView.text = price?.toNumberFormat() ?: ""
        }
    }

    override fun getLayout(): Int = R.layout.item_text_with_price

    override fun initializeViewBinding(view: View): ItemTextWithPriceBinding =
        ItemTextWithPriceBinding.bind(view)
}
