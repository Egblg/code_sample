package com.egblg.apps.bars.detailorder.domain

import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.data.response.OrderResponse
import com.egblg.apps.bars.database.entities.OrderEntity
import kotlinx.coroutines.flow.Flow

interface OrderDetailInteractor {
    suspend fun getOrder(): OrderEntity?

    suspend fun finishOrder(): Flow<Result<Unit>>

    suspend fun startOrder(): Flow<Result<OrderResponse>>

    suspend fun saveOrder(order: OrderEntity)
}
