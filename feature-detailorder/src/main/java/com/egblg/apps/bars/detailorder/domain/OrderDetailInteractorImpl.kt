package com.egblg.apps.bars.detailorder.domain

import com.egblg.apps.bars.database.AppDatabaseRepository
import com.egblg.apps.bars.database.entities.OrderEntity

class OrderDetailInteractorImpl(
    private val appDatabaseRepository: AppDatabaseRepository,
    private val repository: OrderDetailsRepository
) : OrderDetailInteractor {
    override suspend fun getOrder() = appDatabaseRepository.getOrderAndDelete()

    override suspend fun finishOrder() = repository.finishOrder()

    override suspend fun startOrder() = repository.startOrder()

    override suspend fun saveOrder(order: OrderEntity) = appDatabaseRepository.saveOrder(order)
}
