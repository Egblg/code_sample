package com.egblg.apps.bars.detailorder.di

import com.egblg.apps.bars.detailorder.domain.OrderDetailInteractorImpl
import com.egblg.apps.bars.detailorder.presentation.OrderDetailsPresenter
import org.koin.dsl.module

val orderDetailsModule = module {
    single { OrderDetailInteractorImpl(appDatabaseRepository = get(), repository = get()) }
    single {
        OrderDetailsPresenter(
            router = get(),
            orderDetailsScreens = get(),
            interactor = get(),
            orderUpdatedChannel = get()
        )
    }
}
