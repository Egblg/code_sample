package com.egblg.apps.bars.detailorder.domain

import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.data.response.OrderResponse
import kotlinx.coroutines.flow.Flow

interface OrderDetailsRepository {
    suspend fun finishOrder(): Flow<Result<Unit>>
    suspend fun startOrder(): Flow<Result<OrderResponse>>
}
