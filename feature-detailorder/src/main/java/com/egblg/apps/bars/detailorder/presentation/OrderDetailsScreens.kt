package com.egblg.apps.bars.detailorder.presentation

import ru.terrakok.cicerone.android.support.SupportAppScreen

interface OrderDetailsScreens {
    fun showEditOrder(): SupportAppScreen
    fun showMap(): SupportAppScreen
    fun showChat(): SupportAppScreen
}
