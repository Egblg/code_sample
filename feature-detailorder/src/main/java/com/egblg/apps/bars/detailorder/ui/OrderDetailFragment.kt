package com.egblg.apps.bars.detailorder.ui

import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import com.egblg.apps.bars.common.domain.NavigationProvider
import com.egblg.apps.bars.common.ui.BaseFragment
import com.egblg.apps.bars.common.ui.ItemHeaderServiceInfo
import com.egblg.apps.bars.common.ui.ItemTextWithPrice
import com.egblg.apps.bars.database.entities.OrderEntity
import com.egblg.apps.bars.database.entities.PaymentMethodEntity
import com.egblg.apps.bars.database.entities.UserEntity
import com.egblg.apps.bars.detailorder.R
import com.egblg.apps.bars.detailorder.presentation.OrderDetailsPresenter
import com.egblg.apps.bars.detailorder.presentation.OrderDetailsView
import com.sixhands.detailorder.databinding.FragmentOrderDetailBinding
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Section
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import org.koin.android.ext.android.get
import org.koin.android.ext.android.inject
import splitties.resources.str
import splitties.toast.toast

class OrderDetailFragment : BaseFragment(R.layout.fragment_order_detail), OrderDetailsView {

    private val binding: FragmentOrderDetailBinding by viewBinding()

    private val navigationProvider: NavigationProvider by inject()

    @InjectPresenter
    lateinit var presenter: OrderDetailsPresenter

    @ProvidePresenter
    fun providePresenter() = get<OrderDetailsPresenter>()

    private val servicesAdapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        setListeners()
    }

    private fun initRecyclerView() {
        binding.servicesRv.adapter = servicesAdapter
    }

    private fun setListeners() {
        with(binding) {
            mainButton.onClick { presenter.onMainButtonClick() }
            toolbar.setNavigationOnClickListener {
                presenter.onMenuClick(navigationProvider)
            }
            toolbar.setOnMenuItemClickListener {
                if (it.itemId == R.id.edit) {
                    presenter.onEditClick()
                    true
                } else false
            }
        }
    }

    override fun toggleLoading(visible: Boolean) {
        binding.loading.root.visible(visible)
    }

    override fun showOrderFinishedMsg() {
        toast(R.string.order_completed)
    }

    override fun showOrderButton(isActiveOrder: Boolean) {
        binding.mainButton.setText(if (isActiveOrder) R.string.order_completed else R.string.start_order)
    }

    override fun showOrderDetails(data: OrderEntity) {
        with(binding) {
            addressTextView.text = data.address
            orderIdTextView.text = String.format(str(R.string.number_sign_placeholder), data.id)
            gosNumberTextView.text = data.gosNumber
            layoutTotalPrice.priceTextView.text = data.totalPrice?.toNumberFormat()
            layoutTopBar.mapButton.onClick {
                if (data.lat != null && data.lon != null) {
                    showNavigatorChooser(data.lat!!, data.lon!!)
                }
            }
            layoutTopBar.chatButton.onClick { presenter.onChatClick() }
        }

        val sections = data.services.mapIndexed { index, priceService ->
            Section().apply {
                setHeader(
                    ItemHeaderServiceInfo(
                        index + 1,
                        priceService.name,
                        priceService.coef.map { Pair(it.name, it.value) },
                        requireContext()
                    )
                )
                priceService.operations?.map {
                    ItemTextWithPrice(
                        it.name,
                        it.price
                    )
                }?.let { update(it) }
                add(ItemTextWithPrice(str(R.string.total), priceService.price))
            }
        }
        servicesAdapter.update(sections)
        servicesAdapter.add(
            ItemTextWithPrice(str(R.string.departure_of_the_brigade), data.callBrigade)
        )
    }

    override fun showPayMethod(paymentMethod: PaymentMethodEntity?) {
        binding.layoutPaymentMethod.textView.apply {
            text = if (paymentMethod == null) {
                str(R.string.cash)
            } else {
                "${paymentMethod.cardType} *${paymentMethod.last4Numbers}"
            }
            setCompoundDrawables(null, null, null, null)
        }
    }

    override fun showUserData(data: UserEntity?) {
        with(binding) {
            userNameTextView.text = data?.userName
            layoutTopBar.chatButton.onClick { presenter.onChatClick() }
            layoutTopBar.phoneButton.onClick {
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:+${data?.userPhone}"))
                startActivity(intent)
            }
            layoutTopBar.layoutAvatar.imageView.load(IMAGE_HOST + data?.userPhotoPath) {
                transformations(CircleCropTransformation())
                error(R.drawable.ic_profile)
                placeholder(R.drawable.ic_profile)
            }
        }
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
