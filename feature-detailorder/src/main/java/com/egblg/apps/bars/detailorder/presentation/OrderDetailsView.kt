package com.egblg.apps.bars.detailorder.presentation

import com.egblg.apps.bars.common.presentation.BaseView
import com.egblg.apps.bars.database.entities.OrderEntity
import com.egblg.apps.bars.database.entities.PaymentMethodEntity
import com.egblg.apps.bars.database.entities.UserEntity
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.SingleStateStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface OrderDetailsView : BaseView {
    fun showOrderDetails(data: OrderEntity)
    fun showUserData(data: UserEntity?)

    @StateStrategyType(SingleStateStrategy::class)
    fun toggleLoading(visible: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showOrderFinishedMsg()
    fun showPayMethod(paymentMethod: PaymentMethodEntity?)
    fun showOrderButton(isActiveOrder: Boolean)
}
