package com.egblg.apps.bars.detailorder.presentation

import com.egblg.apps.bars.common.data.Result
import com.egblg.apps.bars.common.domain.NavigationProvider
import com.egblg.apps.bars.common.presentation.BasePresenter
import com.egblg.apps.bars.database.domain.OrderUpdatedChannel
import com.egblg.apps.bars.database.entities.OrderEntity
import com.egblg.apps.bars.database.entities.toOrderEntity
import com.egblg.apps.bars.detailorder.domain.OrderDetailInteractor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
class OrderDetailsPresenter(
    private val router: Router,
    private val orderDetailsScreens: OrderDetailsScreens,
    private val interactor: OrderDetailInteractor,
    orderUpdatedChannel: OrderUpdatedChannel
) : BasePresenter<OrderDetailsView>() {

    private var viewModel: OrderEntity? = null
    private val orderUpdatedChannel = orderUpdatedChannel.openSubscription()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        presenterScope.launch { listenOrderUpdatedChannel() }
    }

    fun onCreate() {
        presenterScope.launch {
            viewModel = interactor.getOrder()
            showOrderData()
        }
    }

    private suspend fun listenOrderUpdatedChannel() {
        for (event in orderUpdatedChannel) {
            viewModel = event
            showOrderData()
        }
    }

    private fun showOrderData() {
        if (viewModel != null) {
            viewState.showUserData(viewModel!!.user)
            viewState.showOrderDetails(viewModel!!)
            viewState.showPayMethod(viewModel?.paymentMethod)
            processStatusOrder()
        }
    }

    private fun processStatusOrder() {
        when (viewModel!!.status) {
            OrderStatus.ON_THE_WAY -> viewState.showOrderButton(false)
            OrderStatus.ACTIVE -> viewState.showOrderButton(true)
        }
    }

    fun onMenuClick(navigationProvider: NavigationProvider) {
        navigationProvider.showDrawer()
    }

    fun onEditClick() {
        presenterScope.launch {
            viewModel?.let {
                interactor.saveOrder(it)
            }
            router.navigateTo(orderDetailsScreens.showEditOrder())
        }
    }

    fun onMainButtonClick() {
        when (viewModel?.status) {
            OrderStatus.ON_THE_WAY -> presenterScope.launch {
                interactor.startOrder().collect {
                    when (it) {
                        is Result.Success -> {
                            viewModel = it.data.toOrderEntity()
                            viewState.showOrderButton(true)
                        }
                        is Result.Error -> viewState.showNetworkConnectionError()
                    }
                }
            }
            OrderStatus.ACTIVE -> presenterScope.launch {
                viewState.toggleLoading(true)
                interactor.finishOrder().collect {
                    viewState.toggleLoading(false)
                    when (it) {
                        is Result.Success -> {
                            viewModel = null
                            viewState.showOrderFinishedMsg()
                            router.newRootChain(orderDetailsScreens.showMap())
                        }
                        is Result.Error -> viewState.showNetworkConnectionError()
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        router.exit()
    }

    fun onChatClick() {
        router.navigateTo(orderDetailsScreens.showChat())
    }
}
